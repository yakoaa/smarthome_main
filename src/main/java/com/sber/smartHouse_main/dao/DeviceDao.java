package com.sber.smartHouse_main.dao;

import com.sber.smartHouse_main.model.Device;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface DeviceDao extends CrudRepository<Device, Long> {

    Device findByNameOrderById(String name);
}
