package com.sber.smartHouse_main.dao;

import com.sber.smartHouse_main.model.DeviceEvents;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;

public interface DeviceEventsDao extends CrudRepository<DeviceEvents, Long> {

    ArrayList<DeviceEvents> findByExecutedOrderByWaTime(boolean executed);

    ArrayList<DeviceEvents> findByExecutedAndWaTimeOrderByWaTime(boolean executed, LocalDateTime WaTime);
}
