package com.sber.smartHouse_main.dao;

import com.sber.smartHouse_main.model.SensorEventRule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorEventRuleDao extends CrudRepository<SensorEventRule, Long> {

    SensorEventRule findByName(String name);
}
