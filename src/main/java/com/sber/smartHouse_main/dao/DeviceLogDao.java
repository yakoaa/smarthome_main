package com.sber.smartHouse_main.dao;

import com.sber.smartHouse_main.model.DeviceLog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;

@Repository
public interface DeviceLogDao extends CrudRepository<DeviceLog, Long> {

    //переделать, запрос не выдает то, что мне нужно
    //а впрочем, все ок
    @Query(value = "select DL from DeviceLog DL where DL.created = (select max(subDL.created) from DeviceLog subDL where subDL.name = DL.name)")
    ArrayList<DeviceLog> getLastData();

    @Query(value = "select DL from DeviceLog DL where DL.name = :name and DL.created between :dateFrom and :dateTo")
    ArrayList<DeviceLog> getDataBetweenByName(@Param("name") String name, @Param("dateFrom") LocalDateTime dateFrom, @Param("dateTo") LocalDateTime dateTo);
}
