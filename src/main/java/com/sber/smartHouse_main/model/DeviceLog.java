package com.sber.smartHouse_main.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Calendar;

@Entity
public class DeviceLog {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String state;
    //@Column
    //private String dataType;

    @Column
    private Long dataValue;

    @Column
    @DateTimeFormat(pattern ="dd.MM.yyyy HH:mm:ss")
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private String comment;

    @Column
    private LocalDateTime created;


    public DeviceLog() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getDataValue() {
        return dataValue;
    }

    public void setDataValue(Long dataValue) {
        this.dataValue = dataValue;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
}
