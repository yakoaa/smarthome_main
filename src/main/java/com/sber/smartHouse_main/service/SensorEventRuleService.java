package com.sber.smartHouse_main.service;

import com.sber.smartHouse_main.dao.SensorEventRuleDao;
import com.sber.smartHouse_main.model.Device;
import com.sber.smartHouse_main.model.DeviceType;
import com.sber.smartHouse_main.model.SensorEventRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class SensorEventRuleService {
    @Autowired
    SensorEventRuleDao sensorEventRuleDao;
    @Autowired
    SensorEventRuleService sensorEventRuleService;

    @Autowired
    DeviceService deviceService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public ArrayList<SensorEventRule> getAll(){
        return (ArrayList<SensorEventRule>) sensorEventRuleDao.findAll();
    }

    public SensorEventRule getOneRule(Long id){
        return sensorEventRuleDao.findById(id).get();
    }

    public void deleteRule(Long id){
        sensorEventRuleDao.deleteById(id);
    }

    public void updateRule(Long id){

        SensorEventRule sensorEventRule = sensorEventRuleService.getOneRule(id);
        if(sensorEventRule.isActive()){
            sensorEventRule.setActive(false);
        } else {
            sensorEventRule.setActive(true);
        }
        sensorEventRuleDao.save(sensorEventRule);
    }

    public void addRule(SensorEventRule sensorEventRule){
        sensorEventRuleDao.save(sensorEventRule);
    }

    public boolean checkLastAddedLog(String name){
        logger.info("check device "+ name +" begins");
        Device dev = deviceService.getOneByName(name);
        logger.info("Device found by name: "+ dev.getName() +" " + dev.getState() +" "+dev.getType());
        if(!dev.equals(null)){
            if(dev.getType().equals(DeviceType.SENSOR.toString())){
                logger.info("checklog if passed");
                return true;
            }
        }
        logger.info("checklog if not passed");
        return false;
    }

    public SensorEventRule getRuleBySensorName(String name){
        return sensorEventRuleDao.findByName(name);
    }
}
