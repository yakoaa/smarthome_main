package com.sber.smartHouse_main.service;

import com.sber.smartHouse_main.dao.DeviceLogDao;
import com.sber.smartHouse_main.model.DeviceEvents;
import com.sber.smartHouse_main.model.DeviceLog;
import com.sber.smartHouse_main.model.SensorEventRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class DeviceLogService {

    @Autowired
    private DeviceLogDao deviceLogDao;

    @Autowired
    private SensorEventRuleService sensorEventRuleService;

    @Autowired
    private DeviceEventsService deviceEventsService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public ArrayList<DeviceLog> getLastData(){

        return deviceLogDao.getLastData();
    }

    public ArrayList<DeviceLog> getDataBetweenByName(String name, LocalDateTime dateFrom, LocalDateTime dateTo){
        return deviceLogDao.getDataBetweenByName(name, dateFrom, dateTo);
    }

    public void addDeviceLog(DeviceLog deviceLog){
        deviceLogDao.save(deviceLog);
        logger.info("log for "+ deviceLog.getName() +" saved" );
        if(sensorEventRuleService.checkLastAddedLog(deviceLog.getName())){
            logger.info("check for " + deviceLog.getName() + " passed, go search rule");
            SensorEventRule sensorEventRule = sensorEventRuleService.getRuleBySensorName(deviceLog.getName());
            if (sensorEventRule == null){
                logger.info("rule isn't found.");
                return;
            }
            logger.info("Device: "+ deviceLog.getName() +" value: "+ deviceLog.getDataValue() +", rule for "+ sensorEventRule.getName() +" value: "+ sensorEventRule.getValue());
            if(sensorEventRule.getValue() < deviceLog.getDataValue()){

                logger.info("if passed");
                DeviceEvents deviceEvents = new DeviceEvents();
                logger.info("device created");
                deviceEvents.setMessage(sensorEventRule.getMessage());
                logger.info("device message set");
                deviceEvents.setExecuted(false);
                logger.info("device executed set");
                deviceEvents.setWaTime(deviceLog.getCreated());

                logger.info("new event for "+ deviceLog.getName() +" saved");
                deviceEventsService.addEvent(deviceEvents);
            }
        }
    }
}
