package com.sber.smartHouse_main.service;

import com.sber.smartHouse_main.dao.DeviceDao;
import com.sber.smartHouse_main.model.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class DeviceService {

    @Autowired
    private DeviceDao deviceDao;

    public ArrayList<Device> getDevices(){ return (ArrayList<Device>) deviceDao.findAll(); }

    public Device getOneByName(String name){return deviceDao.findByNameOrderById(name);}
}
