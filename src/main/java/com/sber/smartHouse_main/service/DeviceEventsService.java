package com.sber.smartHouse_main.service;

import com.sber.smartHouse_main.dao.DeviceEventsDao;
import com.sber.smartHouse_main.model.Device;
import com.sber.smartHouse_main.model.DeviceEvents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Service
public class DeviceEventsService {

    @Autowired
    private DeviceEventsDao deviceEventsDao;

    public ArrayList<DeviceEvents> getScheduledEvents(){
        return deviceEventsDao.findByExecutedOrderByWaTime(false);
    }
    public ArrayList<DeviceEvents> getScheduledEventsForExecute(){
        //получаем текущую дату и время
        Clock clock = Clock.system(ZoneId.of("Europe/Moscow"));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(clock.instant(), ZoneId.of("Europe/Moscow"));

        //вычитаем секунды, ибо не особо нужны, записи делаются в базу без них
        localDateTime = localDateTime.minusSeconds(localDateTime.getSecond());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String formattedDateTime = localDateTime.format(formatter);

        localDateTime = LocalDateTime.parse(formattedDateTime, formatter);

        ArrayList<DeviceEvents> deviceEvents = deviceEventsDao.findByExecutedOrderByWaTime(false);
        ArrayList<DeviceEvents> deviceEventsForReturn = new ArrayList<DeviceEvents>();
        for(DeviceEvents deviceEventForSearch: deviceEvents){
            if(deviceEventForSearch.getWaTime() == null || deviceEventForSearch.getWaTime().isEqual(localDateTime) || deviceEventForSearch.getWaTime().isBefore(localDateTime)){
                deviceEventsForReturn.add(deviceEventForSearch);
                deviceEventForSearch.setExecuted(true);
                deviceEventsDao.save(deviceEventForSearch);
            }

        }

        return deviceEventsForReturn;
    }

    public void addEvent(DeviceEvents deviceEvent){
        deviceEventsDao.save(deviceEvent);
    }

    public void updateEvent(DeviceEvents deviceEvents){
        deviceEventsDao.save(deviceEvents);
    }
}
