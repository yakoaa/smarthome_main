package com.sber.smartHouse_main.service;

import com.sber.smartHouse_main.model.DeviceLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Service
public class KafkaConsumer {

    @Autowired
    private DeviceLogService deviceLogService;

    //раскомментировать строку, когда перейду на линукс с рабочей кафкой
    @KafkaListener(topics = "smart_home", groupId = "group_id")
    public void consume(String message) throws IOException {
       //сюда добавить логику обработки сообщений из кафки
        DeviceLog deviceLog = new DeviceLog();
        String[] messArr;
        System.out.println(message);
        messArr = message.split("#");
        deviceLog.setName(messArr[0]);
        deviceLog.setState(messArr[1]);
        deviceLog.setDataValue(Long.valueOf(messArr[2]));
        deviceLog.setComment(messArr[3]);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withLocale(Locale.US);
        String dateTime = messArr[4];
        LocalDateTime localDateTime = LocalDateTime.parse(dateTime, formatter);

        deviceLog.setCreated(localDateTime);

        deviceLogService.addDeviceLog(deviceLog);
    }
}
