package com.sber.smartHouse_main.controller;

import com.sber.smartHouse_main.model.SensorEventRule;
import com.sber.smartHouse_main.service.SensorEventRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/smart-home/main/api/device/event/rule")
public class SensorEventRuleController {

    @Autowired
    private SensorEventRuleService sensorEventRuleService;

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<SensorEventRule>> getAll(){
        return new ResponseEntity<ArrayList<SensorEventRule>>((ArrayList<SensorEventRule>) sensorEventRuleService.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SensorEventRule> getOneByName(@PathVariable("name") String name){
        return new ResponseEntity<SensorEventRule>(sensorEventRuleService.getRuleBySensorName(name), HttpStatus.OK);
    }

    @PostMapping(path = "/add", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addRule(@RequestBody SensorEventRule sensorEventRule){
        sensorEventRuleService.addRule(sensorEventRule);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PutMapping(path = "/change/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> changeStatus(@PathVariable Long id){
        sensorEventRuleService.updateRule(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
