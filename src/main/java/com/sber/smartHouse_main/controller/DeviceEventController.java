package com.sber.smartHouse_main.controller;

import com.sber.smartHouse_main.model.DeviceEvents;
import com.sber.smartHouse_main.service.DeviceEventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/smart-home/main/api/device/event")
public class DeviceEventController {

    @Autowired
    private DeviceEventsService deviceEventsService;

    @GetMapping(path = "/scheduled")
    public ResponseEntity<ArrayList<DeviceEvents>> getAllEvents(){
        return new ResponseEntity<ArrayList<DeviceEvents>>(deviceEventsService.getScheduledEvents(), HttpStatus.OK);
    }

    @GetMapping(path = "/execute")
    public ResponseEntity<ArrayList<DeviceEvents>> getAllForExecute(){
        return new ResponseEntity<ArrayList<DeviceEvents>>(deviceEventsService.getScheduledEventsForExecute(), HttpStatus.OK);
    }

    @PostMapping(path = "/edit", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> editEvent(@RequestBody DeviceEvents deviceEvents){
        deviceEventsService.updateEvent(deviceEvents);
        return new ResponseEntity(HttpStatus.OK);
    }
}
