package com.sber.smartHouse_main.controller;

import com.sber.smartHouse_main.model.DeviceLog;
import com.sber.smartHouse_main.service.DeviceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;

@RestController
@RequestMapping("/smart-home/main/api/device/log")
public class DeviceLogController {

    @Autowired
    DeviceLogService deviceLogService;


    @GetMapping(path = "/last_data",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<DeviceLog>> getLastData(){
        return new ResponseEntity<ArrayList<DeviceLog>>(deviceLogService.getLastData(), HttpStatus.OK);
    }

    @PostMapping(path = "/add_log",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addLog(@RequestBody DeviceLog deviceLog){
            deviceLogService.addDeviceLog(deviceLog);
            return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping(path = "/get_by",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<DeviceLog>> getDataByNameAndTime(
            @RequestParam String name,
            @RequestParam("from") LocalDateTime dateFrom,
            @RequestParam("to") LocalDateTime dateTo){
        return new ResponseEntity<ArrayList<DeviceLog>>(deviceLogService.getDataBetweenByName(name, dateFrom, dateTo), HttpStatus.OK);
    }
}
