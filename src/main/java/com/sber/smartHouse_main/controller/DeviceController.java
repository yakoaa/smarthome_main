package com.sber.smartHouse_main.controller;

import com.sber.smartHouse_main.model.Device;
import com.sber.smartHouse_main.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
@RequestMapping("/smart-home/main/api/device/")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<Device>> getDevices(){
        return new ResponseEntity<ArrayList<Device>>(deviceService.getDevices(), HttpStatus.OK);
    }

    @GetMapping(value = "/one/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Device> getDevByName(@PathVariable String name){

        return new ResponseEntity<Device>(deviceService.getOneByName(name), HttpStatus.OK);
    }

    @PutMapping(value = "/change/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> changeState(@PathVariable("name") String name){
        Device device = deviceService.getOneByName(name);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity <String> entity = new HttpEntity<String>(headers);

        return (ResponseEntity<?>)restTemplate.exchange("http://localhost:8082/smart-home/extend/api/device/" + device.getId(), HttpMethod.PUT, entity, ResponseEntity.class).getBody();
    }
}
