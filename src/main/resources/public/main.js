window.onload = function afterload(){
	let notifTimer = setInterval(notif, 60000);
}

function notif(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4){
			if (this.status == 200) {
				//добавить логику отображения уведомлений
				var resp = JSON.parse(xhttp.responseText);
				if(resp.length > 0){
					let div_notif = document.getElementsByClassName("smartui-notification")[0];
					let div_msg = document.getElementsByClassName("smartui-notif-message")[0];
					let i;
					for (i=0;i<resp.length; i++){
						let div_submsg = document.createElement('div');
						div_submsg.className = 'smartui-notif-submessage';
						if(resp[i]['waTime'] == null)
						    resp[i]['waTime'] = 'Сейчас';
						div_submsg.innerText = 'Дата: ' + resp[i]["waTime"] + ' Сообщение: ' + resp[i]["message"];
						div_msg.appendChild(div_submsg);
					}
					div_notif.appendChild(div_msg);
					div_notif.style.display = 'unset';
				}
			}
			else
			{
				alert('Не удалось подключиться к серверу или произошла ошибка: ' + this.responseText);
			}
		}
	};
	
	xhttp.open("GET", "http://localhost:8081/smart-home/main/api/device/event/execute", true);
	xhttp.send();
}

function showView1(id){
	closeView1()
	let div = document.getElementById(id);
	div.style.display = 'unset';
}

function closeView1(){
	let div_buttons = document.getElementsByClassName('smartui-extended');
	let i;
	for (i=0;i<div_buttons.length;i++)
		div_buttons[i].style.display = 'none';
	let div_view = document.getElementsByClassName('smartui-extended-second');
	for (i=0;i<div_view.length;i++)
		div_view[i].style.display = 'none';
}
function showView2(id){
	closeView2()
	let div = document.getElementById(id);
	div.style.display = 'unset';
}

function closeView2(){
	let div_view = document.getElementsByClassName('smartui-extended-second');
	let i;
	for (i=0;i<div_view.length;i++)
		div_view[i].style.display = 'none';
}

function closeNotif(){
	let div_view = document.getElementsByClassName('smartui-notification');
	let i;
	
	let notif_msg = document.getElementsByClassName('smartui-notif-submessage');
	for (i=0;i<notif_msg.length;)
		notif_msg[0].remove();
	
	for (i=0;i<div_view.length;i++)
		div_view[i].style.display = 'none';
}

function changeState(x){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4){
			if (this.status == 200 || this.status == 202) {
				alert('Изменение прошло успешно');
			}
			else
			{
				alert(this.responseText);
			}
		}
	};
	xhttp.open("PUT", x, true);
	xhttp.send();
}

function addSchedule(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4){
			if (this.status == 200) {
				alert('Изменение прошло успешно');
			}
			else
			{
				alert(this.responseText);
			}
		}
	};
	
	let schedule = {
		executed: false,
		message: document.getElementsByClassName('smartui-schedule-message')[0].value,
		waTime: document.getElementsByClassName('smartui-schedule-date-from')[0].value
	};
	let json = JSON.stringify(schedule);
	
	xhttp.open("POST", "http://localhost:8081/smart-home/main/api/device/event/edit", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send(json);
}

function addRule(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(this.readyState == 4){
			if (this.status == 201) {
				alert('Изменение прошло успешно');
			}
			else
			{
				alert(this.responseText);
			}
		}
	};

	let rule = {
		active: true,
		message: document.getElementsByClassName('smartui-rule-message')[0].value,
		name: document.getElementsByClassName('smartui-rule-device')[0].value,
		value: document.getElementsByClassName('smartui-rule-data')[0].value
	};
	let json = JSON.stringify(rule);

	xhttp.open("POST", "http://localhost:8081/smart-home/main/api/device/event/rule/add", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xhttp.send(json);
}

function refresh(id){
	//chrome://flags/#out-of-blink-cors 
	var xhttp = new XMLHttpRequest();
	var k;
	var url = null;
	var urls = [
			['smartui-log-last', 'http://localhost:8081/smart-home/main/api/device/log/last_data/'],
			['smartui-device-info', 'http://localhost:8081/smart-home/main/api/device/all/'],
			['smartui-scheduled-info', 'http://localhost:8081/smart-home/main/api/device/event/scheduled'],
			['smartui-rule-info', 'http://localhost:8081/smart-home/main/api/device/event/rule/all'],
			['smartui-device-info-one', 'http://localhost:8081/smart-home/main/api/device/one/']
		];
	for (k=0;k<urls.length;k++){
		if(urls[k][0] == id){
			url = urls[k][1];
			break;
		}
	}		
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4) 
			if (this.status == 200) {
				var resp = JSON.parse(xhttp.responseText);
				if(id == 'smartui-device-info-one'){
					resp = [resp];
				}

				let oldTable = document.getElementsByClassName('smartui-table');
                if(oldTable.length > 0){
                    oldTable[0].remove()
                }

				if(resp.length > 0){
					let div = document.getElementById(id);
					var tbl = document.createElement("table");
					var tblBody = document.createElement("tbody");
					
					tbl.className  = 'smartui-table';
					tblBody.className  = 'smartui-table-body';
					
					var i;
					var j;
					var keys = Object.keys(resp[0]);
					
					for(i=0;i<1;i++){
						var row = document.createElement("tr");
						row.className  = 'smartui-table-row-header smartui-table-row';
						
						for (j=0;j<keys.length;j++){
							var cell = document.createElement("th");
							cell.className = 'smartui-table-row-cell-header smartui-table-row-cell-header-'+keys[j];
							cell.innerText = keys[j];
							row.appendChild(cell);
						}
						
						if(id == 'smartui-device-info' || id =='smartui-device-info-one' || id == 'smartui-rule-info'){
							var cell = document.createElement("th");
							cell.className = 'smartui-table-row-cell-header smartui-table-rowcell-header-button';
							cell.innerText = 'management';
							row.appendChild(cell);
						}
						
						tblBody.appendChild(row);
					}
					
					for(i=0;i<resp.length;i++){
						var row = document.createElement("tr");
						row.className  = 'smartui-table-row';
						
						for (j=0;j<keys.length;j++){
							var cell = document.createElement("td");
							cell.className = 'smartui-table-row-cell smartui-table-row-cell-'+keys[j];
							cell.innerText = resp[i][keys[j]];
							row.appendChild(cell);
						}
						
						if(id == 'smartui-device-info' || id =='smartui-device-info-one'){
							var cell = document.createElement("td");
							cell.className = 'smartui-table-row-cell smartui-table-row-cell-'+keys[j];
							var tableButton = document.createElement("button");
							tableButton.innerText = 'Сменить состояние';

							var name = resp[i][keys[1]];
							tableButton.className = 'smartui-device-button-'+name;
							tableButton.setAttribute("onclick","changeState('http://localhost:8081/smart-home/main/api/device/change/"+name+"')");
							cell.appendChild(tableButton);
							row.appendChild(cell);
						}

                        if(id == 'smartui-rule-info'){
							var cell = document.createElement("td");
							cell.className = 'smartui-table-row-cell smartui-table-row-cell-'+keys[j];
							var tableButton = document.createElement("button");
							tableButton.innerText = 'Сменить состояние';

							var rowId = resp[i][keys[0]];
							tableButton.className = 'smartui-rule-button-'+rowId;
							tableButton.setAttribute("onclick","changeState('http://localhost:8081/smart-home/main/api/device/event/rule/change/"+rowId+"')");
							cell.appendChild(tableButton);
							row.appendChild(cell);
						}

						tblBody.appendChild(row);
					}
					tbl.appendChild(tblBody);
					div.appendChild(tbl);		
				}
			}
			else
			{
				alert("Произошла ошибка, нет доступа к серверу.");
			}
	};
	
	
	if(id == 'smartui-device-info-one'){
		url = url + document.getElementsByClassName('smartui-device-name')[0].value;
	}
	
	
	if(url != null){
		xhttp.open("GET", url, true);
		xhttp.send();
	}
}